#ifndef S21_TEST_MATRIX
#define S21_TEST_MATRIX

#include <check.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "../s21_matrix.h"

#define TEST_ACCURACY 0.000001

Suite* s21_test_create_matrix();
Suite* s21_test_remove_matrix();
Suite* s21_test_eq_matrix();
Suite* s21_test_sum_matrix();
Suite* s21_test_sub_matrix();
Suite* s21_test_mult_number();
Suite* s21_test_mult_matrix();
Suite* s21_test_transpose();
Suite* s21_test_calc_complements();
Suite* s21_test_determinant();
Suite* s21_test_inverse_matrix();

void run_test();
void random_matrix(matrix_t* test);
void eq_matrix(matrix_t* temp_1, matrix_t* temp_2);
double random_double(double* res);

#endif
