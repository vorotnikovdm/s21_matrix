#include "s21_test_matrix.h"

START_TEST(test_create_matrix_normal) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(3, 3, &temp) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_normal_2) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(4, 2, &temp) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_normal_3) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(1, 1, &temp) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_normal_4) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(13, 17, &temp) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_wrong) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(-1, 3, &temp) == 1);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_wrong_2) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(3, -1, &temp) == 1);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_wrong_3) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(-1, -1, &temp) == 1);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_wrong_4) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(0, 0, &temp) == 1);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_wrong_5) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(3, 0, &temp) == 1);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_create_matrix_wrong_6) {
  matrix_t temp = {NULL, 0, 0};
  ck_assert(s21_create_matrix(0, 3, &temp) == 1);
  s21_remove_matrix(&temp);
}
END_TEST

Suite *s21_test_create_matrix() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_create_matrix");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_create_matrix_normal);
  tcase_add_test(tc_core, test_create_matrix_normal_2);
  tcase_add_test(tc_core, test_create_matrix_normal_3);
  tcase_add_test(tc_core, test_create_matrix_normal_4);
  tcase_add_test(tc_core, test_create_matrix_wrong);
  tcase_add_test(tc_core, test_create_matrix_wrong_2);
  tcase_add_test(tc_core, test_create_matrix_wrong_3);
  tcase_add_test(tc_core, test_create_matrix_wrong_4);
  tcase_add_test(tc_core, test_create_matrix_wrong_5);
  tcase_add_test(tc_core, test_create_matrix_wrong_6);

  suite_add_tcase(s, tc_core);
  return s;
}
