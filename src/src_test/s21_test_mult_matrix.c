#include "s21_test_matrix.h"

START_TEST(test_mult_matrix_normal) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 0);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_normal_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(2, 2, &temp_1);
  s21_create_matrix(2, 2, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 0);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_normal_3) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(10, 10, &temp_1);
  s21_create_matrix(10, 10, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 0);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_normal_4) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  eq_matrix(&temp_1, &temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 0);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_wrong) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 2, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_wrong_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(2, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_wrong_3) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(0, 0, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_wrong_4) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(0, 0, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_mult_matrix(&temp_1, &temp_2, &res) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_matrix_example) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0},
           expected = {NULL, 0, 0};
  s21_create_matrix(3, 2, &temp_1);
  s21_create_matrix(2, 3, &temp_2);
  s21_create_matrix(3, 3, &expected);
  temp_1.matrix[0][0] = 1;
  temp_1.matrix[0][1] = 4;
  temp_1.matrix[1][0] = 2;
  temp_1.matrix[1][1] = 5;
  temp_1.matrix[2][0] = 3;
  temp_1.matrix[2][1] = 6;
  temp_2.matrix[0][0] = 1;
  temp_2.matrix[0][1] = -1;
  temp_2.matrix[0][2] = 1;
  temp_2.matrix[1][0] = 2;
  temp_2.matrix[1][1] = 3;
  temp_2.matrix[1][2] = 4;
  expected.matrix[0][0] = 9;
  expected.matrix[0][1] = 11;
  expected.matrix[0][2] = 17;
  expected.matrix[1][0] = 12;
  expected.matrix[1][1] = 13;
  expected.matrix[1][2] = 22;
  expected.matrix[2][0] = 15;
  expected.matrix[2][1] = 15;
  expected.matrix[2][2] = 27;
  s21_mult_matrix(&temp_1, &temp_2, &res);
  ck_assert(s21_eq_matrix(&res, &expected) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
  s21_remove_matrix(&expected);
}
END_TEST

Suite *s21_test_mult_matrix() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_mult_matrix");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_mult_matrix_normal);
  tcase_add_test(tc_core, test_mult_matrix_normal_2);
  tcase_add_test(tc_core, test_mult_matrix_normal_3);
  tcase_add_test(tc_core, test_mult_matrix_normal_4);
  tcase_add_test(tc_core, test_mult_matrix_wrong);
  tcase_add_test(tc_core, test_mult_matrix_wrong_2);
  tcase_add_test(tc_core, test_mult_matrix_wrong_3);
  tcase_add_test(tc_core, test_mult_matrix_wrong_4);
  tcase_add_test(tc_core, test_mult_matrix_example);

  suite_add_tcase(s, tc_core);
  return s;
}
