#include "s21_test_matrix.h"

START_TEST(test_remove_matrix_normal) {
  matrix_t temp = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp);
  s21_remove_matrix(&temp);
  ck_assert(temp.matrix == NULL);
}
END_TEST

START_TEST(test_remove_matrix_normal_2) {
  matrix_t temp = {NULL, 4, 2};
  s21_create_matrix(3, 3, &temp);
  s21_remove_matrix(&temp);
  ck_assert(temp.matrix == NULL);
}
END_TEST

START_TEST(test_remove_matrix_normal_3) {
  matrix_t temp = {NULL, 1, 1};
  s21_create_matrix(3, 3, &temp);
  s21_remove_matrix(&temp);
  ck_assert(temp.matrix == NULL);
}
END_TEST

START_TEST(test_remove_matrix_normal_4) {
  matrix_t temp = {NULL, 13, 17};
  s21_create_matrix(3, 3, &temp);
  s21_remove_matrix(&temp);
  ck_assert(temp.matrix == NULL);
}
END_TEST

Suite *s21_test_remove_matrix() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_remove_matrix");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_remove_matrix_normal);
  tcase_add_test(tc_core, test_remove_matrix_normal_2);
  tcase_add_test(tc_core, test_remove_matrix_normal_3);
  tcase_add_test(tc_core, test_remove_matrix_normal_4);

  suite_add_tcase(s, tc_core);
  return s;
}
