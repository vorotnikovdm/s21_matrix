#include "s21_test_matrix.h"

START_TEST(test_determinant_normal) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(3, 3, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_normal_2) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(1, 1, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_normal_3) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(2, 2, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_normal_4) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(5, 5, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_normal_5) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(10, 10, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 0);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_wrong) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(0, 0, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 1);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_wrong_2) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(1, 3, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 2);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_wrong_3) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0;
  s21_create_matrix(3, 1, &temp);
  random_matrix(&temp);
  ck_assert(s21_determinant(&temp, &res) == 2);
  s21_remove_matrix(&temp);
}
END_TEST

START_TEST(test_determinant_example) {
  matrix_t temp = {NULL, 0, 0};
  double res = 0.0, expected = 0.0;
  s21_create_matrix(3, 3, &temp);
  temp.matrix[0][0] = 1;
  temp.matrix[0][1] = 2;
  temp.matrix[0][2] = 3;
  temp.matrix[1][0] = 4;
  temp.matrix[1][1] = 5;
  temp.matrix[1][2] = 6;
  temp.matrix[2][0] = 7;
  temp.matrix[2][1] = 8;
  temp.matrix[2][2] = 9;
  s21_determinant(&temp, &res);
  ck_assert_float_eq_tol(res, expected, TEST_ACCURACY);
  s21_remove_matrix(&temp);
}
END_TEST

Suite *s21_test_determinant() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_determinant");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_determinant_normal);
  tcase_add_test(tc_core, test_determinant_normal_2);
  tcase_add_test(tc_core, test_determinant_normal_3);
  tcase_add_test(tc_core, test_determinant_normal_4);
  tcase_add_test(tc_core, test_determinant_normal_5);
  tcase_add_test(tc_core, test_determinant_wrong);
  tcase_add_test(tc_core, test_determinant_wrong_2);
  tcase_add_test(tc_core, test_determinant_wrong_3);
  tcase_add_test(tc_core, test_determinant_example);

  suite_add_tcase(s, tc_core);
  return s;
}
