#include "s21_test_matrix.h"

START_TEST(test_transpose_normal) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 2, &temp);
  random_matrix(&temp);
  ck_assert(s21_transpose(&temp, &res) == 0);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_transpose_wrong) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  ck_assert(s21_transpose(&temp, &res) == 1);
}

START_TEST(test_transpose_example) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0}, expected = {NULL, 0, 0};
  s21_create_matrix(3, 2, &temp);
  temp.matrix[0][0] = 1;
  temp.matrix[0][1] = 4;
  temp.matrix[1][0] = 2;
  temp.matrix[1][1] = 5;
  temp.matrix[2][0] = 3;
  temp.matrix[2][1] = 6;
  s21_create_matrix(2, 3, &expected);
  expected.matrix[0][0] = 1;
  expected.matrix[0][1] = 2;
  expected.matrix[0][2] = 3;
  expected.matrix[1][0] = 4;
  expected.matrix[1][1] = 5;
  expected.matrix[1][2] = 6;
  s21_transpose(&temp, &res);
  ck_assert(s21_eq_matrix(&res, &expected) == 1);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
  s21_remove_matrix(&expected);
}
END_TEST

Suite *s21_test_transpose() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_transpose");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_transpose_normal);
  tcase_add_test(tc_core, test_transpose_wrong);
  tcase_add_test(tc_core, test_transpose_example);

  suite_add_tcase(s, tc_core);
  return s;
}
