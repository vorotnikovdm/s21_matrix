#include "s21_test_matrix.h"

START_TEST(test_inverse_matrix_normal) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp);
  random_matrix(&temp);
  ck_assert(s21_inverse_matrix(&temp, &res) == 0);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_inverse_matrix_normal_2) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(5, 5, &temp);
  random_matrix(&temp);
  ck_assert(s21_inverse_matrix(&temp, &res) == 0);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_inverse_matrix_normal_3) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(10, 10, &temp);
  random_matrix(&temp);
  ck_assert(s21_inverse_matrix(&temp, &res) == 0);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_inverse_matrix_wrong) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(0, 0, &temp);
  random_matrix(&temp);
  ck_assert(s21_inverse_matrix(&temp, &res) == 1);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_inverse_matrix_wrong_2) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(1, 3, &temp);
  random_matrix(&temp);
  ck_assert(s21_inverse_matrix(&temp, &res) == 2);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_inverse_matrix_wrong_3) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 1, &temp);
  random_matrix(&temp);
  ck_assert(s21_inverse_matrix(&temp, &res) == 2);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_inverse_matrix_example) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0}, expected = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp);
  temp.matrix[0][0] = 2;
  temp.matrix[0][1] = 5;
  temp.matrix[0][2] = 7;
  temp.matrix[1][0] = 6;
  temp.matrix[1][1] = 3;
  temp.matrix[1][2] = 4;
  temp.matrix[2][0] = 5;
  temp.matrix[2][1] = -2;
  temp.matrix[2][2] = -3;
  s21_inverse_matrix(&temp, &res);
  s21_create_matrix(3, 3, &expected);
  expected.matrix[0][0] = 1;
  expected.matrix[0][1] = -1;
  expected.matrix[0][2] = 1;
  expected.matrix[1][0] = -38;
  expected.matrix[1][1] = 41;
  expected.matrix[1][2] = -34;
  expected.matrix[2][0] = 27;
  expected.matrix[2][1] = -29;
  expected.matrix[2][2] = 24;
  ck_assert(s21_eq_matrix(&res, &expected) == 1);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
  s21_remove_matrix(&expected);
}
END_TEST

Suite *s21_test_inverse_matrix() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_inverse_matrix");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_inverse_matrix_normal);
  tcase_add_test(tc_core, test_inverse_matrix_normal_2);
  tcase_add_test(tc_core, test_inverse_matrix_normal_3);
  tcase_add_test(tc_core, test_inverse_matrix_wrong);
  tcase_add_test(tc_core, test_inverse_matrix_wrong_2);
  tcase_add_test(tc_core, test_inverse_matrix_wrong_3);
  tcase_add_test(tc_core, test_inverse_matrix_example);

  suite_add_tcase(s, tc_core);
  return s;
}
