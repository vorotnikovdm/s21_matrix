#include "s21_test_matrix.h"

START_TEST(test_mult_number_normal) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp);
  int check = 0;
  double mult = random_double(&mult);
  random_matrix(&temp);
  ck_assert(s21_mult_number(&temp, mult, &res) == 0);
  for (int i = 0; i < temp.rows; i++) {
    for (int j = 0; j < temp.columns; j++) {
      if (temp.matrix[i][j] * mult - res.matrix[i][j] > TEST_ACCURACY)
        check = 1;
    }
  }
  ck_assert(check == 0);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_number_wrong) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(0, 3, &temp);
  double mult = random_double(&mult);
  ck_assert(s21_mult_number(&temp, mult, &res) == 1);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_number_wrong_2) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 0, &temp);
  double mult = random_double(&mult);
  ck_assert(s21_mult_number(&temp, mult, &res) == 1);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_mult_number_example) {
  matrix_t temp = {NULL, 0, 0}, res = {NULL, 0, 0}, expected = {NULL, 0, 0};
  double mult = 2.0;
  s21_create_matrix(3, 3, &temp);
  s21_create_matrix(3, 3, &expected);
  temp.matrix[0][0] = 1;
  temp.matrix[0][1] = 2;
  temp.matrix[0][2] = 3;
  temp.matrix[1][0] = 0;
  temp.matrix[1][1] = 4;
  temp.matrix[1][2] = 2;
  temp.matrix[2][0] = 2;
  temp.matrix[2][1] = 3;
  temp.matrix[2][2] = 4;
  expected.matrix[0][0] = 2;
  expected.matrix[0][1] = 4;
  expected.matrix[0][2] = 6;
  expected.matrix[1][0] = 0;
  expected.matrix[1][1] = 8;
  expected.matrix[1][2] = 4;
  expected.matrix[2][0] = 4;
  expected.matrix[2][1] = 6;
  expected.matrix[2][2] = 8;
  s21_mult_number(&temp, mult, &res);
  ck_assert(s21_eq_matrix(&res, &expected) == 1);
  s21_remove_matrix(&temp);
  s21_remove_matrix(&res);
  s21_remove_matrix(&expected);
}
END_TEST

Suite *s21_test_mult_number() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_mult_number");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_mult_number_normal);
  tcase_add_test(tc_core, test_mult_number_wrong);
  tcase_add_test(tc_core, test_mult_number_wrong_2);
  tcase_add_test(tc_core, test_mult_number_example);
  suite_add_tcase(s, tc_core);
  return s;
}
