#include "s21_test_matrix.h"

START_TEST(test_sum_matrix_normal) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sum_matrix(&temp_1, &temp_2, &res) == 0);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sum_matrix_normal_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0},
           res_2 = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  eq_matrix(&temp_1, &temp_2);
  s21_sum_matrix(&temp_1, &temp_2, &res);
  s21_mult_number(&temp_1, 2.0, &res_2);
  ck_assert(s21_eq_matrix(&res, &res_2) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
  s21_remove_matrix(&res_2);
}
END_TEST

START_TEST(test_sum_matrix_wrong) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(2, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sum_matrix(&temp_1, &temp_2, &res) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sum_matrix_wrong_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 2, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sum_matrix(&temp_1, &temp_2, &res) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sum_matrix_wrong_3) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(0, 0, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sum_matrix(&temp_1, &temp_2, &res) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sum_matrix_wrong_4) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(0, 0, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sum_matrix(&temp_1, &temp_2, &res) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sum_matrix_example) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0},
           expected = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  s21_create_matrix(3, 3, &expected);
  temp_1.matrix[0][0] = 1;
  temp_1.matrix[0][1] = 2;
  temp_1.matrix[0][2] = 3;
  temp_1.matrix[1][0] = 0;
  temp_1.matrix[1][1] = 4;
  temp_1.matrix[1][2] = 5;
  temp_1.matrix[2][0] = 0;
  temp_1.matrix[2][1] = 0;
  temp_1.matrix[2][2] = 6;
  temp_2.matrix[0][0] = 1;
  temp_2.matrix[0][1] = 0;
  temp_2.matrix[0][2] = 0;
  temp_2.matrix[1][0] = 2;
  temp_2.matrix[1][1] = 0;
  temp_2.matrix[1][2] = 0;
  temp_2.matrix[2][0] = 3;
  temp_2.matrix[2][1] = 4;
  temp_2.matrix[2][2] = 1;
  expected.matrix[0][0] = 2;
  expected.matrix[0][1] = 2;
  expected.matrix[0][2] = 3;
  expected.matrix[1][0] = 2;
  expected.matrix[1][1] = 4;
  expected.matrix[1][2] = 5;
  expected.matrix[2][0] = 3;
  expected.matrix[2][1] = 4;
  expected.matrix[2][2] = 7;
  s21_sum_matrix(&temp_1, &temp_2, &res);
  ck_assert(s21_eq_matrix(&res, &expected) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&expected);
  s21_remove_matrix(&res);
}
END_TEST

Suite *s21_test_sum_matrix() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_sum_matrix");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_sum_matrix_normal);
  tcase_add_test(tc_core, test_sum_matrix_normal_2);
  tcase_add_test(tc_core, test_sum_matrix_wrong);
  tcase_add_test(tc_core, test_sum_matrix_wrong_2);
  tcase_add_test(tc_core, test_sum_matrix_wrong_3);
  tcase_add_test(tc_core, test_sum_matrix_wrong_4);
  tcase_add_test(tc_core, test_sum_matrix_example);

  suite_add_tcase(s, tc_core);
  return s;
}
