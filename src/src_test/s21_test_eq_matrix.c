#include "s21_test_matrix.h"

START_TEST(test_eq_matrix_normal) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  eq_matrix(&temp_1, &temp_2);
  ck_assert(s21_eq_matrix(&temp_1, &temp_2) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
}
END_TEST

START_TEST(test_eq_matrix_normal_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0};
  s21_create_matrix(2, 4, &temp_1);
  s21_create_matrix(2, 4, &temp_2);
  random_matrix(&temp_1);
  eq_matrix(&temp_1, &temp_2);
  ck_assert(s21_eq_matrix(&temp_1, &temp_2) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
}
END_TEST

START_TEST(test_eq_matrix_normal_3) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0};
  s21_create_matrix(1, 1, &temp_1);
  s21_create_matrix(1, 1, &temp_2);
  random_matrix(&temp_1);
  eq_matrix(&temp_1, &temp_2);
  ck_assert(s21_eq_matrix(&temp_1, &temp_2) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
}
END_TEST

START_TEST(test_eq_matrix_normal_4) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0};
  s21_create_matrix(10, 13, &temp_1);
  s21_create_matrix(10, 13, &temp_2);
  random_matrix(&temp_1);
  eq_matrix(&temp_1, &temp_2);
  ck_assert(s21_eq_matrix(&temp_1, &temp_2) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
}
END_TEST

START_TEST(test_eq_matrix_wrong) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(2, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_eq_matrix(&temp_1, &temp_2) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
}
END_TEST

START_TEST(test_eq_matrix_wrong_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0};
  s21_create_matrix(3, 2, &temp_1);
  s21_create_matrix(2, 4, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_eq_matrix(&temp_1, &temp_2) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
}
END_TEST

START_TEST(test_eq_matrix_wrong_3) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  sleep(1);
  random_matrix(&temp_2);
  ck_assert(s21_eq_matrix(&temp_1, &temp_2) == 0);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
}
END_TEST

Suite *s21_test_eq_matrix() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_eq_matrix");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_eq_matrix_normal);
  tcase_add_test(tc_core, test_eq_matrix_normal_2);
  tcase_add_test(tc_core, test_eq_matrix_normal_3);
  tcase_add_test(tc_core, test_eq_matrix_normal_4);
  tcase_add_test(tc_core, test_eq_matrix_wrong);
  tcase_add_test(tc_core, test_eq_matrix_wrong_2);
  tcase_add_test(tc_core, test_eq_matrix_wrong_3);

  suite_add_tcase(s, tc_core);
  return s;
}
