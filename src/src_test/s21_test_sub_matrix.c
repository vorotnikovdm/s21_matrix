#include "s21_test_matrix.h"

START_TEST(test_sub_matrix_normal) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sub_matrix(&temp_1, &temp_2, &res) == 0);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sub_matrix_normal_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0},
           res_2 = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  eq_matrix(&temp_1, &temp_2);
  s21_sub_matrix(&temp_1, &temp_2, &res);
  s21_create_matrix(3, 3, &res_2);
  ck_assert(s21_eq_matrix(&res, &res_2) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
  s21_remove_matrix(&res_2);
}
END_TEST

START_TEST(test_sub_matrix_wrong) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(2, 3, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sub_matrix(&temp_1, &temp_2, &res) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sub_matrix_wrong_2) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(3, 2, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sub_matrix(&temp_1, &temp_2, &res) == 2);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sub_matrix_wrong_3) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(0, 0, &temp_1);
  s21_create_matrix(3, 3, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sub_matrix(&temp_1, &temp_2, &res) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

START_TEST(test_sub_matrix_wrong_4) {
  matrix_t temp_1 = {NULL, 0, 0}, temp_2 = {NULL, 0, 0}, res = {NULL, 0, 0};
  s21_create_matrix(3, 3, &temp_1);
  s21_create_matrix(0, 0, &temp_2);
  random_matrix(&temp_1);
  random_matrix(&temp_2);
  ck_assert(s21_sub_matrix(&temp_1, &temp_2, &res) == 1);
  s21_remove_matrix(&temp_1);
  s21_remove_matrix(&temp_2);
  s21_remove_matrix(&res);
}
END_TEST

Suite *s21_test_sub_matrix() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_sub_matrix");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_sub_matrix_normal);
  tcase_add_test(tc_core, test_sub_matrix_normal_2);
  tcase_add_test(tc_core, test_sub_matrix_wrong);
  tcase_add_test(tc_core, test_sub_matrix_wrong_2);
  tcase_add_test(tc_core, test_sub_matrix_wrong_3);
  tcase_add_test(tc_core, test_sub_matrix_wrong_4);

  suite_add_tcase(s, tc_core);
  return s;
}
