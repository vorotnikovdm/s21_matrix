#include "s21_test_matrix.h"

void random_matrix(matrix_t *test) {
  srand((unsigned int)time(NULL));
  double min_value = -10.0, max_value = 10.0;
  for (int i = 0; i < test->rows; i++) {
    for (int j = 0; j < test->columns; j++) {
      double random_value = (double)rand() / RAND_MAX;
      double scaled_value = min_value + random_value * (max_value - min_value);
      test->matrix[i][j] = scaled_value;
    }
  }
}

void eq_matrix(matrix_t *temp_1, matrix_t *temp_2) {
  for (int i = 0; i < temp_1->rows; i++) {
    for (int j = 0; j < temp_1->columns; j++) {
      temp_2->matrix[i][j] = temp_1->matrix[i][j];
    }
  }
}

double random_double(double *res) {
  srand((unsigned int)time(NULL));
  double min_value = -10.0, max_value = 10.0;
  double random_value = (double)rand() / RAND_MAX;
  *res = min_value + random_value * (max_value - min_value);
  return *res;
}
