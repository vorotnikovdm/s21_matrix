#include "s21_matrix.h"

void getMinor(matrix_t A, matrix_t *temp, int row, int col) {
  for (int i = 0, temp_row = 0; i < A.rows; i++) {
    if (i != row) {
      for (int j = 0, temp_col = 0; j < A.columns; j++) {
        if (j != col) {
          temp->matrix[temp_row][temp_col] = A.matrix[i][j];
          temp_col++;
        } else
          continue;
      }
      temp_row++;
    } else
      continue;
  }
}
