#include "s21_matrix.h"

int s21_transpose(matrix_t *A, matrix_t *result) {
  int error = 0;
  if (A->rows <= 0 || A->columns <= 0 || A == NULL || A->matrix == NULL)
    error = 1;
  else {
    if ((error = s21_create_matrix(A->columns, A->rows, result)) == 0) {
      for (int i = 0; i < result->rows; i++) {
        for (int j = 0; j < result->columns; j++) {
          result->matrix[i][j] = A->matrix[j][i];
        }
      }
    }
  }
  return error;
}
