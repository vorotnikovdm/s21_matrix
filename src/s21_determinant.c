#include "s21_matrix.h"

int s21_determinant(matrix_t *A, double *result) {
  int error = 0;
  if (A->rows <= 0 || A->columns <= 0 || A == NULL || A->matrix == NULL)
    error = 1;
  else if (A->rows != A->columns)
    error = 2;
  else {
    if (A->rows == 1)
      *result = A->matrix[0][0];
    else
      *result = getDet(A);
  }
  return error;
}
