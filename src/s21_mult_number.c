#include "s21_matrix.h"

int s21_mult_number(matrix_t *A, double number, matrix_t *result) {
  int error = 0;
  if (A->rows <= 0 || A->columns <= 0 || A == NULL || A->matrix == NULL)
    error = 1;
  else {
    if ((error = s21_create_matrix(A->rows, A->columns, result)) == 0) {
      for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < A->columns; j++) {
          result->matrix[i][j] = A->matrix[i][j] * number;
        }
      }
    }
  }
  return error;
}
