#include "s21_matrix.h"

int s21_inverse_matrix(matrix_t *A, matrix_t *result) {
  int error = 0;
  if (A->rows <= 0 || A->columns <= 0 || A == NULL || A->matrix == NULL)
    error = 1;
  else if (A->rows != A->columns)
    error = 2;
  else {
    double temp_det = 0.0;
    s21_determinant(A, &temp_det);
    if (fabs(temp_det) < ACCURACY)
      error = 2;
    else {
      matrix_t temp_comp = {NULL, 0, 0}, temp_transp = {NULL, 0, 0};
      s21_calc_complements(A, &temp_comp);
      s21_transpose(&temp_comp, &temp_transp);
      s21_create_matrix(A->rows, A->columns, result);
      for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < A->columns; j++) {
          result->matrix[i][j] = temp_transp.matrix[i][j] / temp_det;
        }
      }
      s21_remove_matrix(&temp_comp);
      s21_remove_matrix(&temp_transp);
    }
  }
  return error;
}
