#include "s21_matrix.h"

int s21_create_matrix(int rows, int columns, matrix_t *result) {
  int error = 0;
  if (rows > 0) {
    if (columns > 0) {
      result->rows = rows;
      result->columns = columns;
      if ((result->matrix = malloc(rows * sizeof(double *)))) {
        for (int i = 0; i < rows; i++) {
          if (!(result->matrix[i] = malloc(columns * sizeof(double)))) {
            result->rows = i;
            s21_remove_matrix(result);
            error = 1;
          }
        }
        if (error != 1) {
          for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
              result->matrix[i][j] = 0.0;
            }
          }
        }
      } else
        error = 1;
    } else
      error = 1;
  } else
    error = 1;
  return error;
}
