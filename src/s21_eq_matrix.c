#include "s21_matrix.h"

int s21_eq_matrix(matrix_t *A, matrix_t *B) {
  int error = SUCCESS;
  if (A->rows <= 0 || A->columns <= 0 || B->rows <= 0 || B->columns <= 0 ||
      A == NULL || A->matrix == NULL || B == NULL || B->matrix == NULL)
    error = FAILURE;
  else if (A->rows != B->rows || A->columns != B->columns)
    error = 2;
  else {
    for (int i = 0; i < A->rows; i++) {
      for (int j = 0; j < A->columns; j++) {
        if (fabs(A->matrix[i][j] - B->matrix[i][j]) > EQ_ACCURACY) {
          error = FAILURE;
          break;
        }
      }
    }
  }
  return error;
}
