#ifndef S21_MATRIX
#define S21_MATRIX

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 1
#define FAILURE 0
#define EQ_ACCURACY 0.0000001
#define ACCURACY 0.000001

typedef struct matrix_struct {
  double **matrix;
  int rows;
  int columns;
} matrix_t;

int s21_create_matrix(int rows, int columns, matrix_t *result);
void s21_remove_matrix(matrix_t *A);
int s21_eq_matrix(matrix_t *A, matrix_t *B);
int s21_sum_matrix(matrix_t *A, matrix_t *B, matrix_t *result);
int s21_sub_matrix(matrix_t *A, matrix_t *B, matrix_t *result);
int s21_mult_matrix(matrix_t *A, matrix_t *B, matrix_t *result);
int s21_mult_number(matrix_t *A, double number, matrix_t *result);
int s21_transpose(matrix_t *A, matrix_t *result);
int s21_determinant(matrix_t *A, double *result);
int s21_calc_complements(matrix_t *A, matrix_t *result);
void getMinor(matrix_t A, matrix_t *temp, int row, int col);
double getDet(matrix_t *A);
int s21_inverse_matrix(matrix_t *A, matrix_t *result);
#endif

/*Библиотека должна быть разработана на языке Си стандарта C11 с использованием
компилятора gcc Код библиотеки должен находиться в папке src в ветке develop Не
использовать устаревшие и выведенные из употребления конструкции языка и
библиотечные функции. Обращать внимания на пометки legacy и obsolete в
официальной документации по языку и используемым библиотекам. Ориентироваться на
стандарт POSIX.1-2017 При написании кода необходимо придерживаться Google Style
Оформить решение как статическую библиотеку (с заголовочным файлом s21_matrix.h)
Библиотека должна быть разработана в соответствии с принципами структурного
программирования Перед каждой функцией использовать префикс s21_ Подготовить
полное покрытие unit-тестами функций библиотеки c помощью библиотеки Check
Unit-тесты должны покрывать не менее 80% каждой функции
Предусмотреть Makefile для сборки библиотеки и тестов (с целями all, clean,
test, s21_matrix.a, gcov_report) В цели gcov_report должен формироваться отчёт
gcov в виде html страницы. Для этого unit-тесты должны запускаться с флагами
gcov Матрица должна быть реализована в виде структуры описанной выше

Проверяемая точность дробной части - максимум 6 знаков после запятой.*/
