#include "s21_matrix.h"

double getDet(matrix_t *A) {
  double res = 0.0;
  if (A->rows == 1)
    res = A->matrix[0][0];
  else if (A->rows == 2) {
    res = A->matrix[0][0] * A->matrix[1][1] - A->matrix[0][1] * A->matrix[1][0];
  } else if (A->rows == 3) {
    res = A->matrix[0][0] * A->matrix[1][1] * A->matrix[2][2] +
          A->matrix[0][1] * A->matrix[1][2] * A->matrix[2][0] +
          A->matrix[0][2] * A->matrix[1][0] * A->matrix[2][1] -
          A->matrix[0][2] * A->matrix[1][1] * A->matrix[2][0] -
          A->matrix[0][0] * A->matrix[1][2] * A->matrix[2][1] -
          A->matrix[0][1] * A->matrix[1][0] * A->matrix[2][2];
  } else {
    matrix_t temp = {NULL, A->rows - 1, A->columns - 1};
    s21_create_matrix(temp.rows, temp.columns, &temp);
    int sign = 1;
    for (int i = 0; i < A->rows; i++) {
      getMinor(*A, &temp, 0, i);
      res += sign * A->matrix[0][i] * getDet(&temp);
      sign *= -1;
    }
    s21_remove_matrix(&temp);
  }
  return res;
}
