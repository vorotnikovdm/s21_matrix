#include "s21_matrix.h"

int s21_calc_complements(matrix_t *A, matrix_t *result) {
  int error = 0;
  if (A->rows <= 0 || A->columns <= 0 || A == NULL || A->matrix == NULL)
    error = 1;
  else if (A->rows != A->columns)
    error = 2;
  else {
    if ((error = s21_create_matrix(A->rows, A->columns, result)) == 0) {
      if (A->rows != 1) {
        for (int i = 0; i < result->rows; i++) {
          for (int j = 0; j < result->columns; j++) {
            matrix_t temp = {NULL, A->rows - 1, A->columns - 1};
            s21_create_matrix(temp.rows, temp.columns, &temp);
            getMinor(*A, &temp, i, j);
            result->matrix[i][j] = getDet(&temp) * pow(-1, i + j);
            s21_remove_matrix(&temp);
          }
        }
      } else
        result->matrix[0][0] = 1;
    }
  }
  return error;
}
