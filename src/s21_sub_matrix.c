#include "s21_matrix.h"

int s21_sub_matrix(matrix_t *A, matrix_t *B, matrix_t *result) {
  int error = 0;
  if (A->rows <= 0 || A->columns <= 0 || B->rows <= 0 || B->columns <= 0 ||
      A == NULL || A->matrix == NULL || B == NULL || B->matrix == NULL)
    error = 1;
  else if (A->rows != B->rows || A->columns != B->columns)
    error = 2;
  else {
    if ((error = s21_create_matrix(A->rows, B->columns, result)) == 0) {
      for (int i = 0; i < result->rows; i++) {
        for (int j = 0; j < result->columns; j++) {
          result->matrix[i][j] = A->matrix[i][j] - B->matrix[i][j];
        }
      }
    }
  }
  return error;
}
